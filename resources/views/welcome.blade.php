<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">

        <style type="text/css" media="screen">
            body {
                padding-top: 100px;
            }
            #formdata {
                width: 30%;
                border: 1px solid #ddd;
                margin: 0 auto;
                padding: 20px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            @if ($products)
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Quantity in stock</th>
                        <th>Price per item</th>
                        <th>Datetime submitted</th>
                        <th>Total value number</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0; ?>
                    @foreach ($products as $product)
                    <tr>
                        <td>{{$product['product_name']}}</td>
                        <td>{{$product['quantity']}}</td>
                        <td>{{$product['price']}}</td>
                        <td>{{$product['created_at']}}</td>
                        <td>{{$product['price'] * $product['quantity']}}</td>
                    </tr>
                        <?php $total = $total + ($product['price'] * $product['quantity']); ?>
                    @endforeach
                    <tr>
                        <td colspan="5" class="text-center">Total: <?= $total; ?></td>
                    </tr>
                </tbody>
            </table>
            @else
                <div class="alert alert-danger">
                    No data found
                </div>
            @endif
            <form id="formdata" accept-charset="utf-8">
                <div class="form-group">
                    <label>Product name</label>
                    <input type="text" class="form-control" name="product_name">
                </div>
                <div class="form-group">
                    <label>Quantity in stock</label>
                    <input type="text" class="form-control" name="quantity">
                </div>
                <div class="form-group">
                    <label>Price per item</label>
                    <input type="text" class="form-control" name="price">
                </div>

                <button type="button" class="btn btn-primary" id="submitbtn" onclick="proceed()">Submit</button>
            </form>
        </div>

        <script type="text/javascript" src="/js/jquery.min.js"></script>

        <script type="text/javascript">
            window.proceed = function () {
                var form = $("#formdata");
                var submitbtn = $("#submitbtn");
                var data = {
                    product_name: form.find('input[name=product_name]').val(),
                    quantity: form.find('input[name=quantity]').val(),
                    price: form.find('input[name=price]').val(),
                    _token: "{{ csrf_token() }}"
                };

                submitbtn.addClass('disabled');

                $.ajax({
                    type: 'POST',
                    url: '/create-product',
                    data: data,
                    success: function(response) {
                        submitbtn.removeClass('disabled');
                        location.reload();
                    }
                });
            }
        </script>
    </body>
</html>
