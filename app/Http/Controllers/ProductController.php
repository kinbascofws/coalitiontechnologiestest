<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;

class ProductController extends Controller
{
	private $datafile;

	public function __construct()
	{
		$this->datafile = url('/products.json');
	}

	public function index()
	{
		return view('welcome')->with(['products' => $this->getData()]);
	}

	public function store(Request $request)
	{
		$inputs = $request->all();

		$data = ($this->getData()) ? $this->getData() : [];

		$data[] = [
			'id' => $this->generateID(),
			'product_name' => $inputs['product_name'],
			'quantity' => $inputs['quantity'],
			'price' => $inputs['price'],
			'created_at' => date('Y-m-d h:i:s', time())
		];

		$this->putData($data);

		return response()->json(['products' => $this->getData()], 201);
	}

	protected function generateID()
	{
		$data = $this->getData();

		if ($data === null) {
			return 1;
		} else {
			$ids = [];
			foreach ($data as $product) {
				$ids[] = $product['id'];
			}

			return max($ids) + 1;
		}
	}

	protected function putData($data)
	{
		if (!file_exists(public_path() . '/products.json')) {
			File::put(public_path() . '/products.json', '');
		}

		return file_put_contents(public_path() . '/products.json', json_encode($data));
	}

	protected function getData()
	{
		if (!file_exists(public_path() . '/products.json')) {
			File::put(public_path() . '/products.json', '');
		}

		$data = json_decode(file_get_contents($this->datafile), true);

		for ($i=0;$i<count($data);$i++) {
			for($x=$i+1;$x<count($data);$x++) {
				if ($data[$x] > $data[$i]) {
					$temp = $data[$i];
					$data[$i] = $data[$x];
					$data[$x] = $temp;
				}
			}
		}

		return $data;
	}
}