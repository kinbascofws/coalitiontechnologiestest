<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'form', 'uses' => '\App\Http\Controllers\ProductController@index']);


Route::post('/create-product', ['as' => 'create', 'uses' => '\App\Http\Controllers\ProductController@store']);